namespace PhysicsBacking
{
    using UnityEngine;

    public struct PhysicsCaughtStamp
    {
        public float Timestamp;
        public Vector3 Position;
        public Quaternion Rotation;
    }
}
