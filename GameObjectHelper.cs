namespace PhysicsBacking
{
    using UnityEngine;

    public static class GameObjectHelper
    {
        public static string GetGameObjectPathWithoutParent(Transform transform)
        {
            string path = transform.name;
            while (transform.parent != null)
            {
                transform = transform.parent;
                if (transform.parent != null)
                {
                    path = transform.name + "/" + path;
                }                
            }
            return path;
        }        
    }
}