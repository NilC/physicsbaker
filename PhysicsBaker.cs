#if UNITY_EDITOR
namespace PhysicsBacking
{
    using System.Collections.Generic;
    using UnityEditor;
    using UnityEngine;

    public class PhysicsBaker : MonoBehaviour
    {
        public Transform RootTransform;
        public float TimeBetweenKeyFrames;
        public string PathToOutputFolder;
        public string AnimationName;

        private Dictionary<Transform, List<PhysicsCaughtStamp>> recordedData = new Dictionary<Transform, List<PhysicsCaughtStamp>>(50);
        private bool isBaking;
        private float currentTimeBetweenKeyFrames;
        private float totalRecordingTime;

        private void Update()
        {
            if (isBaking == false)
            {
                return;
            }

            currentTimeBetweenKeyFrames += Time.deltaTime;
            totalRecordingTime += Time.deltaTime;

            if (currentTimeBetweenKeyFrames >= TimeBetweenKeyFrames)
            {
                MakeStamp();

                currentTimeBetweenKeyFrames = 0.0f;
            }
        }

        public void StartBake()
        {
            isBaking = true;

            Reset();

            EnableAllNestedTransformationAndAddToRecordedData(RootTransform);
            MakeStamp();

            PhysicsBakerPreparer.OnBakeStarted(RootTransform.gameObject);
        }

        private void Reset()
        {
            recordedData.Clear();
            currentTimeBetweenKeyFrames = 0.0f;
            totalRecordingTime = 0.0f;
        }

        private void EnableAllNestedTransformationAndAddToRecordedData(Transform entityTransform)
        {
            for (var i = 0; i < entityTransform.childCount; i++)
            {
                var childTransform = entityTransform.GetChild(i);
                childTransform.gameObject.SetActive(true);
                recordedData.Add(childTransform, new List<PhysicsCaughtStamp>(100));

                EnableAllNestedTransformationAndAddToRecordedData(childTransform);
            }
        }

        public void EndBake()
        {
            isBaking = false;

            CreateAnimation();
        }

        private void MakeStamp()
        {
            foreach (var recordedDataItem in recordedData)
            {
                var currentTrasform = recordedDataItem.Key;
                recordedData[currentTrasform].Add(new PhysicsCaughtStamp
                {
                    Timestamp = totalRecordingTime,
                    Position = currentTrasform.localPosition,
                    Rotation = currentTrasform.localRotation,
                });
            }
        }

        private void CreateAnimation()
        {
            var animationClip = new AnimationClip();
            animationClip.name = AnimationName;

            foreach (var recordedDataItem in recordedData)
            {
                string pathToGameObject = GameObjectHelper.GetGameObjectPathWithoutParent(recordedDataItem.Key);

                var positionXAnimationCurve = new AnimationCurve();
                var positionYAnimationCurve = new AnimationCurve();
                var positionZAnimationCurve = new AnimationCurve();

                var rotationXAnimationCurve = new AnimationCurve();
                var rotationYAnimationCurve = new AnimationCurve();
                var rotationZAnimationCurve = new AnimationCurve();
                var rotationWAnimationCurve = new AnimationCurve();

                foreach (var currentTransform in recordedDataItem.Value)
                {
                    positionXAnimationCurve.AddKey(currentTransform.Timestamp, currentTransform.Position.x);
                    positionYAnimationCurve.AddKey(currentTransform.Timestamp, currentTransform.Position.y);
                    positionZAnimationCurve.AddKey(currentTransform.Timestamp, currentTransform.Position.z);

                    rotationXAnimationCurve.AddKey(currentTransform.Timestamp, currentTransform.Rotation.x);
                    rotationYAnimationCurve.AddKey(currentTransform.Timestamp, currentTransform.Rotation.y);
                    rotationZAnimationCurve.AddKey(currentTransform.Timestamp, currentTransform.Rotation.z);
                    rotationWAnimationCurve.AddKey(currentTransform.Timestamp, currentTransform.Rotation.w);
                }

                AddCurveToAnimationClip(animationClip, pathToGameObject, "m_LocalPosition.x", positionXAnimationCurve);
                AddCurveToAnimationClip(animationClip, pathToGameObject, "m_LocalPosition.y", positionYAnimationCurve);
                AddCurveToAnimationClip(animationClip, pathToGameObject, "m_LocalPosition.z", positionZAnimationCurve);

                AddCurveToAnimationClip(animationClip, pathToGameObject, "m_LocalRotation.x", rotationXAnimationCurve);
                AddCurveToAnimationClip(animationClip, pathToGameObject, "m_LocalRotation.y", rotationYAnimationCurve);
                AddCurveToAnimationClip(animationClip, pathToGameObject, "m_LocalRotation.z", rotationZAnimationCurve);
                AddCurveToAnimationClip(animationClip, pathToGameObject, "m_LocalRotation.w", rotationWAnimationCurve);
            }

            string path = string.Format("Assets/{0}/{1}.anim", PathToOutputFolder, AnimationName);
            AssetDatabase.CreateAsset(animationClip, path);
            AssetDatabase.SaveAssets();
            AssetDatabase.Refresh();
        }

        private void AddCurveToAnimationClip(AnimationClip animationClip, string pathToGameObject, string propertyName, AnimationCurve animationCurve)
        {
            var positionXCurveBinding = EditorCurveBinding.FloatCurve(pathToGameObject, typeof(Transform), propertyName);
            AnimationUtility.SetEditorCurve(animationClip, positionXCurveBinding, animationCurve);
        }
    }
}
#endif